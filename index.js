let http = require("http");

// Create server
http.createServer(function(request, response){
	response.writeHead(200, {'Content-Type': 'text-plain'});

	response.end("Hello Fake World!");
}).listen(4000);

console.log('Server running at localhost: 4000');

// Create another dummy server
http.createServer(function(request, response){
	response.writeHead(200, {'Content-Type': 'text-plain'});

	response.end("Hello mf!")
}).listen(8000);
